---
title: "How to Contribute"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The first step to getting involved is joining our [mailing list](https://accounts.eclipse.org/mailing-list/ospo.zone).

You can also collaborate with the OSPO Alliance on several assets:

- The OSPO.Zone website, which is managed as [an open source Eclipse project](https://www.eclipse.org/projects/handbook/). The Eclipse Plato project is still under creation. Visit the [project proposal](https://projects.eclipse.org/proposals/eclipse-plato), express your interest, and get ready to contribute!

- The [OW2 Good Governance Initiative](https://www.ow2.org/view/OSS_Governance/), the blueprint for establishing OSPOs, developed by OW2 to help implement corporate-wide open source policies, and set up OSPOs. Please follow [these instructions](https://www.ow2.org/view/OSS_Governance/About#HHowtoContribute) to contribute to the GGI.

- Got a mature OSPO already? We need case studies about best practices and, perhaps unexpectedly, worst practices (things you did you regretted…)
