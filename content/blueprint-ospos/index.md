---
title: "The blueprint for establishing OSPOs"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The OSPO Alliance is built out of the OSS Good Governance blueprint developed by European open source organisation OW2 to help implement corporate-wide open source policies, and set up OSPOs. The methodology proposes a comprehensive approach based on five objectives inspired by a motivational behaviour model{{< note-number number="1" >}}.

![motivational behaviour model image](/images/position-paper-motivational-behaviour-model.png)

- **Usage Goal**: This Goal is about using OSS and developing related competences while using OSS. It covers technical ability and experience with OSS, plus developing some understanding and awareness of OSS.
- **Trust Goal**: This Goal is about the secure and responsible use of OSS. It covers in particular compliance and dependency management policies. It is about aiming for the state of the art in implementing the right processes.
- **Culture Goal**: This Goal is concerned with developing an OSS culture that will encourage and support best practices. It's about being part of the open source community, sharing experience and being recognized. An individual perspective that contributes at an organisational level.
- **Engagement Goal**: This Goal develops the corporate perspective. Contributing back to open source projects and supporting open source communities. Developing project visibility: communicating and participating in open source industry as well as community events. At this level, the enterprise engages with the OSS ecosystem and contributes to its sustainability.
- **Strategy Goal**: With this Goal the organisation embraces the full potential of OSS. It proactively uses OSS for innovation and competitiveness. It leverages OSS as an enabler of digital transformation and digital sovereignty. C-level open source awareness is achieved.

---
{{< note-number number="1" >}}
{{< note-content content=`Abraham Maslow, 1943 paper "` url="http://psychclassics.yorku.ca/Maslow/motivation.htm" title="A Theory of Human Motivation" other-content=`" in Psychological Review`>}}

v1.0, © 2021 OSPO Alliance & authors, licensed under CC-BY-4.0

Authors: Cédric Thomas (OW2)
